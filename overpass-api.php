<?php

$query = $_SERVER["QUERY_STRING"];
$url = 'http://overpass-api.de/api/interpreter?' . $query;
#$url = 'http://overpass.osm.rambler.ru/cgi/interpreter?' . $query;

$cache_file_rel = 'cache/overpass-'. md5($query) . ".json";;
$cache_file = dirname(__FILE__) . '/' . $cache_file_rel;

// garder la valeur ci-dessous synchronisee avec cache/.htaccess
$cache_lifetime = '-7 days';
$data = '';
if (!file_exists($cache_file) || filemtime($cache_file) < strtotime($cache_lifetime)) { 
  $data = get($url);
  if (strstr($data, '<title>500 Internal Server Error</title>') ||
     strstr($data, '"remark": "runtime error: ')) {
     // do not cache!
     header('Content-type: application/json');
     echo $data;
     exit(0);
  }
  atomic_file_put_contents($cache_file, $data);
}

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: GET");
    header('Access-Control-Max-Age: 86400');
    header('Content-type: application/json');
    if (!$data) $data = file_get_contents($cache_file);
    echo $data;
} else {
    header('Location: ' . $cache_file_rel);
}

function get($url) {

  $session = curl_init($url);

  // Don't return HTTP headers. Do return the contents of the call
  curl_setopt($session, CURLOPT_HEADER, false);
  curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

  return curl_exec($session);
}

function atomic_file_put_contents($file, $content) {
  $tmp_file = $file . ".tmp";
  if (!file_put_contents($tmp_file, $content))
    exit("failed to write $tmp_file (content length is " . strlen($content) . ")");
  if (!rename($tmp_file, $file))
    exit("failed to rename $tmp_file into $file");
}
