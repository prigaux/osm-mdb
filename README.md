La page [Vélocistes parisiens et franciliens](http://mdb-idf.org/velocistes) de MDB est une mise en forme des données OpenStreetMap :
* le format des données dans OSM est expliqué ici : http://wiki.openstreetmap.org/wiki/FR:Tag:shop%3Dbicycle
* [overpass-api](http://wiki.openstreetmap.org/wiki/Overpass_API) est utilisé pour récupérer les vélocistes dans OSM
* la mise en forme est faite en javascript (fichier velocistes.js)
