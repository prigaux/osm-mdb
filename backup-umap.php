<?php

// by default umap only keeps a small number of versions (see LEAFLET_STORAGE_KEEP_VERSIONS used in purge_old_versions)
// we want to keep more so run this in cron to save recent versions from umap

$layerId = $_GET['layerId'];
if (filter_var($layerId, FILTER_VALIDATE_INT) === false) exit("invalid layerId $layerId");

$umap_base_url = "http://umap.openstreetmap.fr/fr/datalayer/$layerId";

$backup_dir = "backup-umap/$layerId";

if (!is_dir($backup_dir)) {
    mkdir($backup_dir);
    mkdir("$backup_dir/versions");

    $list_versions = <<<'EOT'
    <?php

    $versions = [];
    $nb = 0;
    $max = isset($_GET["max"]) ? $_GET["max"] : 0;
    foreach (scandir("..", SCANDIR_SORT_DESCENDING) as $version) {
        if (preg_match("/_(.*)\.geojson$/", $version, $m)) {
            $versions[] = [ "size" => filesize("../$version"),
                            "name" => $version,
                            "at" => $m[1] ];
            $nb++;
            if ($max && $nb > $max) break;
        }
    }

    header("Content-type: application/json; charset=UTF-8");
    echo json_encode([ "versions" => $versions ]);
EOT;
    atomic_file_put_contents("$backup_dir/versions/index.php", $list_versions);
}

$versions = get_json("$umap_base_url/versions/");

foreach ($versions->versions as $e) {
    get_version($e->name, $e->at);
}


function get_version($name, $at) {
    global $backup_dir, $umap_base_url;
    
    $backup_file = "$backup_dir/$name";
    if (!file_exists($backup_file)) {    
        echo "getting $name $at<br>\n";
        $data = get("$umap_base_url/$name");
        atomic_file_put_contents($backup_file, $data);
        touch($backup_file, $at / 1000);
    }
}

function get_json($url) {
    return json_decode(get($url));
}

function get($url) {
    //echo "getting $url\n";
    $session = curl_init($url);

    // Don't return HTTP headers. Do return the contents of the call
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

    return curl_exec($session);
}

function atomic_file_put_contents($file, $content) {
    $tmp_file = $file . ".tmp";
    if (!file_put_contents($tmp_file, $content))
        exit("failed to write $tmp_file (content length is " . strlen($content) . ")");
    if (!rename($tmp_file, $file))
        exit("failed to rename $tmp_file into $file");
}
