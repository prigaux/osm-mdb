(function () {
"use strict";

var zoom=11;
var lat=48.829806825323;
var lon=2.3505082726479;

var attr_osm = 'Map data &copy; <a href="http://openstreetmap.org/">OpenStreetMap</a> contributors';
var attr_overpass = 'POI via <a href="http://www.overpass-api.de/">Overpass API</a>';
var osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', 
          {opacity: 0.7, attribution: [attr_osm, attr_overpass].join(', ')});

var map = new L.Map('map').addLayer(osm).setView(new L.LatLng(lat, lon), zoom);

    
function getContact(tags, kind) {
    return tags["contact:" + kind] || tags["addr:" + kind] || tags[kind];
}

function getService(tags, kind) {
    return tags["service:bicycle:" + kind] || tags[kind] || '';
}

function formatPhone(tags) {
    var s = getContact(tags, 'phone');
    if (!s) return null;
    return "<a class='phone' href='tel:" + s + "'>" + s.replace(/^\+33 ?/, '0') + "</a>";
}

function formatMail(tags) {
    var s = getContact(tags, 'email');
    if (!s) return null;
    return "<a href='mailto:" + s + "'>" + s + "</a>";
}

function notEmpty(l) {
    return l.filter(function (s) { return s });
}

function formatAddr(tags, full) {
    var l = [];
    var street = getContact(tags, 'street');
    if (street) {
	l.push(getContact(tags, 'housenumber'));
	l.push(street);
    }
    if (full) {
      l.push(getContact(tags, 'postcode'));
      l.push(getContact(tags, 'city'));
    }
    return notEmpty(l).join(' ');
}

function format_opening_hours(tags) {
    var v = tags.opening_hours;
    if (!v) return null;

    v = v.replace(/\bMon?\b/i, 'Lun');
    v = v.replace(/\bTue?\b/i, 'Mar');
    v = v.replace(/\bWed?\b/i, 'Mer');
    v = v.replace(/\bThu?\b/i, 'Jeu');
    v = v.replace(/\bFri?\b/i, 'Ven');
    v = v.replace(/\bSat?\b/i, 'Sam');
    v = v.replace(/\bSun?\b/i, 'Dim');

    v = v.replace(/\boff\b/i, 'fermé');
    v = v.replace(/:00/g, 'h');
    v = v.replace(/(\d):(\d\d)/g, '$1h$2');
    v = v.replace(/,\s*/g, ', ');
    v = v.replace(/;\s*/g, '. ');

    return "Horaires : " + v;
}
function formatServices(tags) {
    var l = [];
    var vente = getService(tags, 'retail').match(/yes|fee/);
    var occaz = getService(tags, 'second_hand').match(/yes|only|fee/);
    if (vente) {
	l.push(occaz ? "vente de vélos neufs et d'occasion" : "vente de vélos neufs");
    }
    if (occaz) {
	l.push("vente de vélos d'occasion");
    }
    if (getService(tags, 'repair').match(/yes|free/)) {
	l.push("réparation de vélos");
    }
    if (getService(tags, 'rental').match(/yes|free/)) {
	l.push("location de vélos");
    }
    if (getService(tags, 'pump').match(/yes|free/)) {
	l.push("pompe en libre service");
    }
    if (getService(tags, 'pump').match(/fee/)) {
	l.push("regonflage payant");
    }
    return l.length ? "Services : " + l.join(', ') : '';
}

function formatBrand(tags) {
    var s = tags.brand || '';
    return s ? "Marques : " + s.split(/; */).join(', ') : '';
}

function formatNote(tags) {
    var s = tags.note || '';
    return s ? s : '';
}

function lines(l) {
   return notEmpty(l).join('<br>');
}

function formatForPopup(feature) {
    try {
        var tags = feature.properties.tags || feature.properties;
        var name = "<h3>" + (tags.name || 'Nom inconnu') + "</h3>";
        var url = tags.website;
        if (url) {
	    if (!url.match(/^http/)) url = 'http://' + url;
	    name = "<a target='_blank' href='" + url + "'>" + name + "</a>";
        }
        var zoom = feature.properties.id > 0 ? "<a href='http://www.openstreetmap.org/node/" + feature.properties.id + "'><img class='goto_osm' src='http://wiki.openstreetmap.org/w/images/7/79/Public-images-osm_logo.svg'></a>" : '';
        var html = lines([ name, formatAddr(tags, true), format_opening_hours(tags), formatPhone(tags), formatMail(tags), formatBrand(tags), formatServices(tags), formatNote(tags) ]) + zoom;
        return html; 
    } catch (e) {
	console.log(e);
    }
}  
    
var style = { fillOpacity: 0.6, color: '#3C9A24' };
    
window.velocistes_carte = function (geojson) {
    function onEachFeature(feature, layer) {
        layer.bindPopup(formatForPopup(feature));
    }
    function pointToLayer(feature, latlng) {
        return L.circleMarker(latlng, {radius: 12});
    }

    var markers = L.markerClusterGroup({ maxClusterRadius: 30 }); 
    map.addLayer(markers);
    L.geoJson(geojson, { onEachFeature: onEachFeature, pointToLayer: pointToLayer, style: style }).addTo(markers);
    
};   

})();
