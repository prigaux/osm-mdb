<?php

$query = isset($_GET["q"]) ? $_GET["q"] : $_POST['q'];

header('Content-type: application/json; charset=UTF-8');
echo '[';
$nb = 0;
foreach (explode(";", $query) as $lon_lat) {
    if ($nb++) echo "\n,\n";
    list ($lon, $lat) = explode(',', $lon_lat);
    if (filter_var($lon, FILTER_VALIDATE_FLOAT) === false) exit("invalid lon $lon");
    if (filter_var($lat, FILTER_VALIDATE_FLOAT) === false) exit("invalid lat $lat");
    echo nominatim_reverse("lat=$lat&lon=$lon");
}
echo ']';

function nominatim_reverse($query) {
    $cache_file_rel = "cache/reverse-$query.json";
    $cache_file = dirname(__FILE__) . '/' . $cache_file_rel;
    if (file_exists($cache_file)) {
        $data = file_get_contents($cache_file);
    } else {
        $url = "https://nominatim.openstreetmap.org/reverse?format=json&email=pascal@rigaux.org&$query";
        //echo "// getting $url\n";
        $data = get($url);
        atomic_file_put_contents($cache_file, $data);
    }
    return $data;
}


function get($url) {

  $session = curl_init($url);

  // Don't return HTTP headers. Do return the contents of the call
  curl_setopt($session, CURLOPT_HEADER, false);
  curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($session, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

  return curl_exec($session);
}

function atomic_file_put_contents($file, $content) {
  $tmp_file = $file . ".tmp";
  if (!file_put_contents($tmp_file, $content))
    exit("failed to write $tmp_file");
  if (!rename($tmp_file, $file))
    exit("failed to rename $tmp_file into $file");
}
