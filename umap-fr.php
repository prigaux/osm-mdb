<?php

$url = 'http://umap.openstreetmap.fr/fr/' . $_SERVER["QUERY_STRING"];

$session = curl_init($url);

    // Don't return HTTP headers. Do return the contents of the call
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

    $data = curl_exec($session);

    // The web service returns JSON. Set the Content-Type appropriately
    header("Content-Type: application/json");

    echo $data;
    curl_close($session);
