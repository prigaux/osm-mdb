<?php

global $velocistes_departements, $velocistes_departement2txt, $velocistes_bbox;

$velocistes_departements = [ 75, 92, 93, 94, 78, 91, 95, 77 ];

$velocistes_departement2txt = [
    75 => "Paris",
    77 => "Seine-et-Marne",
    78 => "Yvelines",
    91 => "Essonne",
    92 => "Hauts-de-Seine",
    93 => "Seine-Saint-Denis",
    94 => "Val-de-Marne",
    95 => "Val-d'Oise",
];

$velocistes_bbox = "1.36505126953125,48.554796169223565,3.42498779296875,49.19965350270188";

function getTags($feature) {
    $r = $feature['properties'];
    if (isset($r['tags'])) $r = $r['tags'];
    return $r;
}
    
function getContact($tags, $kind) {
    $r = @$tags["contact:" . $kind];
    if (!$r) $r = @$tags["addr:" . $kind];
    if (!$r) $r = @$tags[$kind];
    return $r;
}

function getService($tags, $kind) {
    $r = @$tags["service:bicycle:" . $kind];
    if (!$r) $r = @$tags[$kind];
    if (!$r) $r = '';
    return $r;
}

function formatPhone($tags) {
    $s = getContact($tags, 'phone');
    if (!$s) return null;
    return "<a class='phone' href='tel:" . $s . "'>" . preg_replace("/^\+33 ?/", '0', $s) . "</a>";
}

function formatMail($tags) {
    $s = getContact($tags, 'email');
    if (!$s) return null;
    return "<a href='mailto:" . $s . "'>" . $s . "</a>";
}

function notEmpty($l) {
    $r = [];
    foreach ($l as $e) {
        if ($e) $r[] = $e;
    }
    return $r;
}

function formatAddr($tags, $full) {
    $l = [];
    $street = getContact($tags, 'street');
    if ($street) {
	$l[] = getContact($tags, 'housenumber');
	$l[] = $street;
    }
    if ($full) {
      $l[] = getContact($tags, 'postcode');
      $l[] = getContact($tags, 'city');
    }
    return implode(' ', notEmpty($l));
}

function format_opening_hours($tags) {
    $v = @$tags['opening_hours'];
    if (!$v) return null;

    $v = preg_replace('/\bMon?\b/i', 'Lun', $v);
    $v = preg_replace('/\bTue?\b/i', 'Mar', $v);
    $v = preg_replace('/\bWed?\b/i', 'Mer', $v);
    $v = preg_replace('/\bThu?\b/i', 'Jeu', $v);
    $v = preg_replace('/\bFri?\b/i', 'Ven', $v);
    $v = preg_replace('/\bSat?\b/i', 'Sam', $v);
    $v = preg_replace('/\bSun?\b/i', 'Dim', $v);

    $v = preg_replace('/\boff\b/i', 'fermé', $v);
    $v = preg_replace('/:00/', 'h', $v);
    $v = preg_replace('/(\d):(\d\d)/', '$1h$2', $v);
    $v = preg_replace('/,\s*/', ', ', $v);
    $v = preg_replace('/;\s*/', '. ', $v);

    return "Horaires : " . $v;
}

function formatServicesShort($tags) {
    $s = '';
    if (preg_match('/yes|fee/', getService($tags, 'retail'))) {
	$s .= "V";
    }
    if (preg_match('/yes|only|fee/', getService($tags, 'second_hand'))) {
        $s .= "O";
    }
    if (preg_match('/yes|free/', getService($tags, 'repair'))) {
        $s .= "R";
    }
    if (preg_match('/yes|free/', getService($tags, 'rental'))) {
        $s .= "L";
    }
    return $s;
}

function formatBrand($tags) {
    $s = @$tags['brand'];
    return $s ? "Marques : " . implode(', ', preg_split('/; */', $s)) : '';
}

function formatNote($tags) {
    $s = @$tags['note'];
    return $s ? $s : '';
}

function lines($l) {
   return implode('<br>', notEmpty($l));
}


function groupByTowns($features) {
    $o = [];

    foreach ($features as $feature) {
        $tags = getTags($feature);

        $postcode = getContact($tags, 'postcode');
        $town = getContact($tags, 'city');
        if (!$town || !$postcode) continue;

        $towncode = urlencode($town);
        if ($town === 'Paris') {
	    $s = preg_replace('/^750*/', '', $postcode) . 'e';
	    if ($s === '1e') $s = '1er';
	    $town = 'Paris ' . $s;
            $towncode = $postcode;
        }
        if (!isset($o[$towncode])) $o[$towncode] = [ 'name' => $town, 'trs' => [], 'postcode' => $postcode ];
        $o[$towncode]['trs'][] = formatShort($feature);
    }    
    return $o;
}

function setContact(&$feature, $kind, $val, $unsafe) {
    if (!$val) return;

    $tags = getTags($feature);
    $existingVal = getContact($tags, $kind);
    if ($existingVal && $val !== $existingVal) {
        if (!$unsafe) {
            $id = $feature['properties']['id'];
            $name = $id > 0 ? "http://www.openstreetmap.org/node/" . $id : $tags['name'];
            error_log("conflicting " . $kind . " for " . $name . " : " . $existingVal . " vs computed " . $val);
        }
    } else {
        $tags['contact:' . $kind] = $val . ($unsafe ? " (?)" : '');
        $feature['properties']['tags'] = $tags;
    }
}
    
function getTownsAndGroupByTowns($features) {
    $missing = [];
    $i = 0;
    foreach($features as $feature) {
        $tags = getTags($feature);

        if (!getContact($tags, 'city') || !getContact($tags, 'postcode')) {
            //echo "missing $i ". $tags['name'] . "\n";
            $missing[implode(',', $feature['geometry']['coordinates'])] = $i;
        }
        $i++;
    }

    $missing_list = array_keys($missing);
    if ($missing_list) {
        //echo "requesting " . implode(';', $missing_list);
        $l = wget_json('http://mdb-idf.org/OSM/nominatim-reverse.php', [ "q" => implode(';', $missing_list) ]);
        $i = 0;
        foreach($l as &$r) {
            $index = $missing[$missing_list[$i++]];
            $feature = $features[$index];
            $address = $r['address'];
            if ($address) {
                $city = @$address['town'];
                if (!$city) $city = @$address['city'];
                if (!$city) $city = @$address['village'];
                if ($city) {
                    setContact($feature, 'city', $city, false);
                    setContact($feature, 'postcode', $address['postcode'], false);
                    setContact($feature, 'street', @$address['road'], true);
                    $features[$index] = $feature;
                }
            }
        }
    }
    return $features;
}
    
function formatShort($feature) {
    try {
        $tags = getTags($feature);
        $name = @$tags['name'];
        if (!$name) $name = 'Nom inconnu';
        $url = @$tags['website'];
        if (!$url) $url = getContact($tags, 'website');
        if (!$url) $url = getContact($tags, 'facebook');
        $id = $feature['properties']['id'];
        $zoom = $id > 0 ? "<a title='voir sur la carte' href='http://www.openstreetmap.org/node/" . $id . "'><img class='goto_osm' src='http://wiki.openstreetmap.org/w/images/7/79/Public-images-osm_logo.svg'></a>" : '';
        if ($url) {
            if (!preg_match('/^http/', $url)) $url = 'http://' . $url;
            $name = "<a target='_blank' href='" . $url . "'>" . $name . "</a>";
        }
        $html = "<tr><td>" . $name . "</td><td>" . formatServicesShort($tags) . "</td><td>" . lines([formatAddr($tags, false), formatPhone($tags), formatMail($tags)]) . "</td><td>" . $zoom . lines([format_opening_hours($tags), formatBrand($tags), formatNote($tags) ]) . "</td></tr>";
        return "<!-- " . @$tags['name'] . " -->" . $html;
    } catch (Exception $e) {
        error_log($e);
    }
}

function townsByDept($towns, $dept_code) {
    $l = array_keys($towns);
    sort($l);
    $r = [];
    foreach ($l as $town) {
        if (substr($towns[$town]['postcode'], 0, 2) == $dept_code)
            $r[] = $town;
    }
    return $r;
}
    
function createLinks($towns) {
    global $velocistes_departements, $velocistes_departement2txt;
    $s = '';
    foreach ($velocistes_departements as $code) {
	$s .= '<img src="/spip/squelettes-dist/puce.gif" class="puce" alt="-" width="8" height="11">';
	$s .= "<a class='spip_ancre' href='#" . $code . "'><strong>" . $velocistes_departement2txt[$code] . "</strong></a>";
	$s .= ' [ ' . implode(' - ', array_map(function ($town) use ($towns) {
	    return "<a class='spip_ancre' href='#" . $town . "'>" . $towns[$town]['name'] . "</a>";    
	}, townsByDept($towns, $code))) . ' ]';
	$s .= "<br>";
    }
    return $s;
}
function createTable($towns) {
    global $velocistes_departements, $velocistes_departement2txt;
    
    $s = '';
    foreach ($velocistes_departements as $dept) {
        $s .= "<p><a name='" . $dept . "'></a><p>";
        $s .= "<h3 class='spip'>" . $velocistes_departement2txt[$dept] . "</h3>";

        foreach (townsByDept($towns, $dept) as $town) {
            $trs = $towns[$town]['trs'];
            sort($trs);
            $trs = implode("\n", $trs);
            $s .= "<table class='spip'><caption><a name='" . $town . "'></a>" . $towns[$town]['name'] . "</caption>";
            $s .= "<tr class='row_first'><th>Nom</th><th>Services</th><th>Coordonnées</th><th>Spécialités</th></tr>";
            $s .= $trs;
            $s .= "</table>";
        }
    }
    return $s;
}

function get_geojson() {
  global $velocistes_bbox;
  $query = "data=[out:json][timeout:300];node[shop=bicycle](bbox);out+body;way(r);out+skel;>;out+skel;&bbox=" . $velocistes_bbox;
  $url = "http://overpass-api.de/api/interpreter?" . $query;
  $url = "http://mdb-idf.org/OSM/overpass-api.php?" . $query;
  //$url = "http://mdb-idf.org/OSM/cache/overpass-b9adb2ace305a3e5e23aad5552a83c47.json";
  
  $geojson1 = json_decode(file_get_contents(__DIR__ . '/new.json'), true);
  $geojson = osmtogeojson(wget_json($url));
  $geojson['features'] = array_merge($geojson['features'], $geojson1['features']);
  return $geojson;
}

function velocistes_doIt($template) {
  $geojson = get_geojson();
  
  $towns = groupByTowns(getTownsAndGroupByTowns($geojson['features'])); 

  $template = str_replace('VELOCISTES_LINKS', '<div id="links">' . createLinks($towns) . '</div>', $template);
  $template = str_replace('VELOCISTES_TABLES', '<div id="tables" class="velocistes">' . createTable($towns) . '</div>', $template);

  $carte_resources = <<<EOS
  <link rel="stylesheet" href="/OSM/leaflet.css">
<link href="/OSM/leaflet-markercluster/v0.4.0/MarkerCluster.css" rel="stylesheet">
<link href="/OSM/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css" rel="stylesheet">
<script src="/OSM/jquery-1.9.0.min.js"></script>
<script src="/OSM/leaflet-src.js"></script>
<script src="/OSM/leaflet-markercluster/v0.4.0/leaflet.markercluster.js"></script>
<script src="/OSM/velocistes-carte.js"></script>
EOS;

  return "<link rel='stylesheet' href='/OSM/velocistes.css'>\n" .
         $template .
         $carte_resources .
         "<script> window.velocistes_carte(" . json_encode($geojson) . ");\n</script>\n";
}

function wget_json($url, $post = null) {
    $session = curl_init($url);

    // Don't return HTTP headers. Do return the contents of the call
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
    if ($post) {
        curl_setopt($session, CURLOPT_POST, 1);
        curl_setopt($session, CURLOPT_POSTFIELDS, http_build_query($post));
    }
    $data = curl_exec($session);
    curl_close($session);
    
    $json = json_decode($data, true);
    //if (!$json) echo "ERR: $data";
    
    return $json;
}

function osmtogeojson($osm) {
    $features = [];

    foreach ($osm['elements'] as $element) {
        if ($element['type'] === 'node') {
            $properties = array_filter($element, function ($k) {
                return !in_array($k, ['lat', 'lon']);
            });
            $features[] = [
                "type"=> "Feature",
                "geometry" => [ "type" => "Point", "coordinates" => [ $element['lon'], $element['lat'] ] ],
                "properties" => $properties,
            ];
        }
    }
    return [ "type" => "FeatureCollection", "features" => $features ];                       
}
